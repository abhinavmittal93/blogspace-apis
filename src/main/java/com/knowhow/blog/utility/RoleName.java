package com.knowhow.blog.utility;

public enum RoleName {
	ROLE_USER, ROLE_ADMIN
}

package com.knowhow.blog.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.knowhow.blog.entities.RoleModel;
import com.knowhow.blog.utility.RoleName;

@Repository

public interface RoleRepository extends JpaRepository<RoleModel, Long> {

	Optional<RoleModel> findByName(RoleName roleName);

}
